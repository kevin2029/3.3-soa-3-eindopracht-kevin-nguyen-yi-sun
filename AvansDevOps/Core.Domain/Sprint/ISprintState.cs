﻿using System;
namespace AvansDevOps.Core.Domain.Sprint
{
    public interface ISprintState
    {

        public void start();
        public void finish();
        public void review();
        public void complete();
    }
}
