﻿using System;
namespace AvansDevOps.Core.Domain
{
    public class example
    {

        public String name;
        public String email;
        public String phoneNumber;
        public String role;

        public example(String name, String email, String phoneNumber, String role) {
            this.name = name;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.role = role;
        }

        public String getName() {

            return name;
        }

        public String getMail()
        {

            return email;
        }
    }
}
